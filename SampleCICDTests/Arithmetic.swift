//
//  Arithmetic.swift
//  SampleCICDTests
//
//  Created by Yiel Miranda on 6/8/21.
//

import Foundation

class Arithmetic {
    let numberOne: Int!
    let numberTwo: Int!

    init(numOne: Int, numTwo: Int) {
        numberOne = numOne
        numberTwo = numTwo
    }

    func addition() -> Int {
        return numberOne + numberTwo
    }
}
